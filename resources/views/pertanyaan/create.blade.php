@extends('master')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-dark ">
        <div class="card-header">
            <h3 class="card-title">Create New Posts</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">judul</label>
                    <input type="text" class="form-control" id="judul" value="{{old('judul','')}}" name="judul" placeholder="Enter judul" required>
                </div>
                @error('judul')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label for="isi">Text</label>
                    <input name="isi" rows="10" cols="10" value="{{old('isi','')}}"  class="form-control" id="isi" placeholder="isi" required></input>
                </div>
                @error('isi')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn bg-dark">Create</button>
            </div>
        </form>
    </div>
</div>
@endsection
